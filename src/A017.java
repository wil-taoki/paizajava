import java.util.Arrays;
import java.util.Scanner;

public class A017 {
    // フィールドの縦幅
    private static int H;
    // フィールドの横幅
    private static int W;
    // 落ちてくる長方形の個数
    private static int N;

    private static int[][] FIELD;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final Integer[] gameInformation = readLineToIntegerArray(scanner);

        // フィールドの縦幅
        H = gameInformation[0];
        // フィールドの横幅
        W = gameInformation[1];
        // 落ちてくる長方形の個数
        N = gameInformation[2];

        FIELD = new int[H][W];

        for (int blockId = 0; blockId < N; blockId++) {
            Integer[] blockInformation = readLineToIntegerArray(scanner);
            // 落ちてくる長方形の縦幅
            int h = blockInformation[0];
            // 落ちてくる長方形の横幅
            int w = blockInformation[1];
            // 落ちてくる長方形の位置
            int x = blockInformation[2];

            int positionedY = calcPositionedY(x, w, h);
            positionBlock(x, positionedY, w, h);
        }

        for (int[] line:FIELD) {
            for (int position:line) {
                if (position == 1) {
                    System.out.print("#");
                }else{
                    System.out.print(".");
                }
            }
            System.out.println();
        }
    }

    private static void positionBlock(int blockX, int blockY, int width, int height) {
        for (int y = blockY; y > blockY - height; y--) {
            for (int x = blockX; x < blockX + width; x++) {
                FIELD[y][x] = 1;
            }
        }
    }

    private static int calcPositionedY(int blockX, int blockWidth, int blockHeight) {
        for (int y = blockHeight; y < H; y++) {
            if (!isBlankLine(blockX, y, blockWidth)) {
                return y - 1;
            }
        }
        return H - 1;
    }

    private static boolean isBlankLine(int x, int y, int width) {
        for (int offset = 0; offset < width; offset++) {
            if (!isBlank( x + offset, y)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isBlank(int x, int y) {
        return FIELD[y][x] == 0;
    }

    /**
     * ストリームから１行読み込みInteger型で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値
     */
    private static Integer readLineToInteger(Scanner scanner) {
        String line = scanner.nextLine();
        return Integer.parseInt(line);
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineToIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
