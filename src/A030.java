import java.util.Arrays;
import java.util.Scanner;

public class A030 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final int N = readLineToInteger(scanner);
        final Integer[] bossIds = readLineToIntegerArray(scanner);

        int subordinates[] = new int[N];

        for (int id = 0; id < N - 1; id++) {
            int bassId = bossIds[id];
            while (bassId != 1) {
                subordinates[bassId-1]++;
                bassId = bossIds[bassId - 2];
            }
            subordinates[0]++;
        }
        for (int subordinate:subordinates) {
            System.out.println(subordinate);

        }
    }

    /**
     * ストリームから１行読み込みInteger型で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値
     */
    private static Integer readLineToInteger(Scanner scanner) {
        String line = scanner.nextLine();
        return Integer.parseInt(line);
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineToIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
