import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


    }

    /**
     * ストリームから１行読み込みInteger型で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値
     */
    private static Integer readLineAsInteger(Scanner scanner) {
        String line = scanner.nextLine();
        return Integer.parseInt(line);
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineAsIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }

    /**
     * ストリームから複数行読み込みInteger型の２次元配列で返す
     * @param scanner 入力ストリーム
     * @param numberOfReadLeadLines 読み込む行数
     * @return 読み込んだ数値の２次元配列
     */
    private static Integer[][] readLinesAsInteger2dArray(Scanner scanner, int numberOfReadLeadLines) {
        Integer[][] lines = new Integer[numberOfReadLeadLines][];
        for(int i = 0; i < numberOfReadLeadLines; i++) {
            lines[i] = readLineAsIntegerArray(scanner);
        }
        return lines;
    }

    /**
     * ストリームから１行読み込みString型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ文字列の配列
     */
    private static String[] readLineAsStringArray(Scanner scanner) {
        String line = scanner.nextLine();
        return line.split(" ");
    }

    /**
     * ストリームから１行読み込みString型のArrayListで返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ文字列の配列
     */
    private static ArrayList<String> readLineAsStringArrayList(Scanner scanner) {
        String line = scanner.nextLine();
        return new ArrayList<>(Arrays.asList(line.split(" ")));
    }

    /**
     * ストリームから複数行読み込みString型の２次元配列で返す
     * @param scanner 入力ストリーム
     * @param numberOfReadLeadLines 読み込む行数
     * @return 読み込んだ文字列の２次元配列
     */
    private static String[][] readLineAsString2dArray(Scanner scanner, int numberOfReadLeadLines) {
        String[][] lines = new String[numberOfReadLeadLines][];
        for(int i = 0; i < numberOfReadLeadLines; i++) {
            lines[i] = readLineAsStringArray(scanner);
        }
        return lines;
    }
}
