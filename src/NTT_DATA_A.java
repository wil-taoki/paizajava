import java.util.*;

public class NTT_DATA_A {
    /**
     * 碁盤の目はN × N
     */
    private static int N;
    /**
     * 先手
     */
    private static final int FIRST = 0;
    /**
     * 黒の碁石
     */
    private static final int BLACK = 0;
    /**
     * 白の碁石
     */
    private static final int WHITE = 1;
    /**
     * 碁石が置かれていない
     */
    private static final int BLANK = -1;
    /**
     * 碁盤の目[Y座標][X座標]
     */
    private static Integer[][] FIELD;
    /**
     * 決着が付く手の座標
     */
    private static class WinPoint {
        // 勝利する碁石の色
        int color;
        // 勝利する手のX座標
        int x;
        // 勝利する手のY座標
        int y;

        WinPoint(int color, int x, int y) {
            this.color = color;
            this.x = x + 1;
            this.y = y + 1;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // 盤面の情報を取得
        Integer[] fieldInformation = readLineAsIntegerArray(scanner);

        // 碁盤の目はN × N
        N = fieldInformation[0];

        // 0:先手、1:後手
        int s = fieldInformation[1];

        // 自分の碁石の色を設定
        int myColor = s == FIRST ? BLACK : WHITE;

        // 盤面の情報を取得
        FIELD = readLinesAsInteger2dArray(scanner, N);

        /*---------デバッグ用---------*/
        /*System.out.println("My color is " + (myColor == BLACK ? "◉" : "○"));
        System.out.println("Opponent color is " + (myColor == BLACK ? "○" : "◉"));
        for (Integer[] row:FIELD) {
            for (Integer col: row) {
                if(col == BLACK) {
                    System.out.print("◉");
                }else if(col == WHITE){
                    System.out.print("○");
                }else{
                    System.out.print("-");
                }
            }
            System.out.println();
        }*/
        /*---------------------------*/

        // 盤面上で決着が付く手の座標を全て探索
        ArrayList<WinPoint> winPoints = getWinPoints();

        // 自分が置かなければ負けてしまう手の座標一覧
        HashSet<String> losePoints = new HashSet<>();

        // 自分が勝てるかどうかのフラグ
        boolean isWin = false;

        // 自分が勝てる手が存在すればその中の１つの手の座標を出力
        for (WinPoint winPoint:winPoints) {
            if (winPoint.color == myColor) {
                System.out.println(winPoint.x + " " + winPoint.y);
                isWin = true;
                break;
            } else {
                losePoints.add(winPoint.x + " " + winPoint.y);
            }
        }

        // 自分が勝てる手が存在しない場合
        if (!isWin) {
            if (losePoints.size() > 1) {
                // 自分が置かなければ負けてしまう手が複数あれば負けのメッセージを出力
                System.out.println("LOSE");
            } else if (losePoints.size() == 1) {
                // 自分が置かなければ負けてしまう手が１つだけであればその手の座標を出力
                Optional<String> a = losePoints.stream().findFirst();
                a.ifPresent(System.out::println);
            } else {
                // 自分が置かなければ負けてしまう手がない場合、碁石が置かれていない座標の中から適当に１つ選んで出力する
                int safeX = 0, safeY = 0;
                for (int y=0; y<N; y++) {
                    for (int x=0; x<N; x++) {
                        if (FIELD[y][x] == BLANK) {
                            safeX = x + 1;
                            safeY = y + 1;
                        }
                    }
                }
                System.out.println(safeX + " " + safeY);
            }
        }

    }

    /**
     * 盤面上で決着が付く手の座標を全て探索する
     * @return 決着が付く手の座標のリスト
     */
    private static ArrayList<WinPoint> getWinPoints() {
        ArrayList<WinPoint> winPoints = new ArrayList<>();

        // 右方向に1列の中で決着が付く手がないかチェック
        for (int y=0; y<N; y++) {
            winPoints.addAll(getWinPointsOfLine(0, y, 1, 0));
        }

        // 下方向に1列の中で決着が付く手がないかチェック
        for (int x=0; x<N; x++) {
            winPoints.addAll(getWinPointsOfLine(x, 0, 0,1));
        }

        // 右斜め下方向に1列の中で決着が付く手がないかチェック
        for (int y=0; y<N-4; y++) {
            winPoints.addAll(getWinPointsOfLine(0, y, 1, 1));
        }
        for (int x=1; x<N-4; x++) {
            winPoints.addAll(getWinPointsOfLine(x, 0, 1,1));
        }

        // 左斜め下方向に1列の中で決着が付く手がないかチェック
        for (int y=0; y<N-4; y++) {
            winPoints.addAll(getWinPointsOfLine(N - 1, y, -1, 1));
        }
        for (int x=4; x<N-2; x++) {
            winPoints.addAll(getWinPointsOfLine(x, 0, -1, 1));
        }

        return winPoints;
    }

    /**
     * 開始座標から探索方向に向かって1列の中で決着が付く手の座標を探索する
     * @param startX 探索開始X座標
     * @param startY 探索開始Y座標
     * @param addX X座標の探索方向
     * @param addY Y座標の探索方向
     * @return 決着が付く手の座標のリスト
     */
    private static ArrayList<WinPoint> getWinPointsOfLine(int startX, int startY, int addX, int addY) {
        int x = startX;
        int y = startY;

        ArrayList<WinPoint> winPoints = new ArrayList<>();

        // 同じ色の連続数
        int consecutiveNumber = 0;
        // 1つ前の碁石の色
        int lastColor = BLANK;

        while (isInsideField(x, y)) {
            int currentColor = FIELD[y][x];

            if (currentColor == BLANK) {
                /* 碁石が置かれていない場合 */

                // この座標の前後に合計４つの同じ色の碁石が並んでいるか場合、決着が付く手の座標として記録
                if (consecutiveNumber > 0 && isConsecutiveSameColor(
                        x + addX,
                        y + addY,
                        addX,
                        addY,
                        lastColor,
                        4 - consecutiveNumber
                )) {
                    // 決着が付く手の座標として記録
                    winPoints.add(new WinPoint(lastColor, x, y));
                    // 探索する座標位置を更新
                    currentColor = lastColor;
                    x += addX * (4 - consecutiveNumber);
                    y += addY * (4 - consecutiveNumber);
                    consecutiveNumber = 4 - consecutiveNumber;
                } else {
                    // 同じ色の連続数をリセット
                    consecutiveNumber = 0;
                }
            } else if(currentColor == lastColor) {
                /* 碁石の色が１つ前と同じ場合 */

                // 同じ色の連続数をインクリメント
                consecutiveNumber++;
            } else {
                /* 碁石の色が１つ前と異なる場合 */

                // 同じ色の連続数を1にリセット
                consecutiveNumber = 1;
            }
            // 1つ前の碁石の色を更新
            lastColor = currentColor;

            // 同じ色の碁石が4つ連続した場合、その両隣に碁石が置かれていないかチェック
            if (consecutiveNumber == 4) {
                // 次の座標の位置を計算
                int nextX = x + addX;
                int nextY = y + addY;

                // 次の座標に碁石が置かれていない場合、決着が付く手の座標として記録
                if (isInsideField(nextX, nextY)) {
                    int nextColor = FIELD[nextY][nextX];
                    if (nextColor == BLANK) {
                        winPoints.add(new WinPoint(currentColor, nextX, nextY));
                    }
                }

                // 4つ前の座標の位置を計算
                int last4X = x -= addX * 4;
                int last4Y = y -= addY * 4;

                // 4つ前の座標に碁石が置かれていない場合、決着が付く手の座標として記録
                if (isInsideField(last4X, last4Y)) {
                    int last4Color = FIELD[last4Y][last4X];
                    if (last4Color == BLANK) {
                        winPoints.add(new WinPoint(currentColor, last4X, last4Y));
                    }
                }
            }

            // 座標位置を更新
            x += addX;
            y += addY;
        }

        return winPoints;
    }

    /**
     * 開始座標から探索方向に向かって指定した色の碁石が指定した個数連続しているかどうか
     * @param startX 探索開始X座標
     * @param startY 探索開始Y座標
     * @param addX X座標の探索方向
     * @param addY Y座標の探索方向
     * @param color 探索する碁石の色
     * @param consecutiveNumber 同じ色の連続数
     * @return true:連続している、false:連続していない
     */
    private static boolean isConsecutiveSameColor(int startX, int startY, int addX, int addY, int color, int consecutiveNumber) {
        int x = startX;
        int y = startY;

        for (int i=0; i<consecutiveNumber; i++) {
            if (!(isInsideField(x, y) && FIELD[y][x] == color)) {
                return false;
            }
            x += addX;
            y += addY;
        }

        return true;
    }

    /**
     * 指定の座標が存在するかどうか
     * @param x X座標
     * @param y Y座標
     * @return 存在する:true、存在しない:false
     */
    private static boolean isInsideField(int x, int y) {
        return x >= 0 && x < N && y >= 0 && y < N;
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineAsIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }

    /**
     * ストリームから複数行読み込みInteger型の２次元配列で返す
     * @param scanner 入力ストリーム
     * @param numberOfReadLeadLines 読み込む行数
     * @return 読み込んだ数値の２次元配列
     */
    private static Integer[][] readLinesAsInteger2dArray(Scanner scanner, int numberOfReadLeadLines) {
        Integer[][] lines = new Integer[numberOfReadLeadLines][];
        for(int i = 0; i < numberOfReadLeadLines; i++) {
            lines[i] = readLineAsIntegerArray(scanner);
        }
        return lines;
    }
}
