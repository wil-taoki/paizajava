import java.util.*;

public class NTT_DATA_B {
    /**
     * 特別ゲストの人数
     */
    private static int M;
    /**
     * 通常ゲストの人数
     */
    private static int N;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // ゲストの人数情報を取得
        Integer[] guestInformation = readLineAsIntegerArray(scanner);
        M = guestInformation[0];
        N = guestInformation[1];

        // 特別ゲスト全員が参加可能な日付一覧
        TreeSet<String> specialGuestsAvailableDates = new TreeSet<>();

        // 日付毎の通常ゲストの参加可能人数
        TreeMap<String, Integer> normalGuestsAvailableNumbers = new TreeMap<>();

        // 特別ゲストが存在する場合特別ゲスト全員が参加可能な日付を集計
        if (M > 0) {
            // 一人目の特別ゲストの参加可能日一覧を特別ゲスト全員が参加可能な日付一覧にセット
            specialGuestsAvailableDates.addAll(getAvailableDates(scanner));

            // 二人目以降の特別ゲストが参加できない日付を特別ゲスト全員が参加可能な日付一覧から削除
            for (int i=1; i<M; i++) {
                // i番目の特別ゲストの参加可能日一覧を取得
                HashSet<String> availableDates = new HashSet<>();
                availableDates.addAll(getAvailableDates(scanner));

                // 削除候補の日付を取得
                ArrayList<String> removeDateList = new ArrayList<>();
                for (String specialGuestsAvailableDate:specialGuestsAvailableDates) {
                    if (!availableDates.contains(specialGuestsAvailableDate)) {
                        removeDateList.add(specialGuestsAvailableDate);
                    }
                }

                // 削除候補の日付を特別ゲスト全員が参加可能な日付一覧から削除
                for (String removeDate:removeDateList) {
                    specialGuestsAvailableDates.remove(removeDate);
                }
            }

            // 日付毎の通常ゲストの参加可能人数を初期化
            for (String specialGuestsAvailableDate:specialGuestsAvailableDates) {
                normalGuestsAvailableNumbers.put(specialGuestsAvailableDate, 0);
            }
        }

        if (N == 0) {
            // 通常ゲストが0人の場合特別ゲスト全員が参加可能な最速の日付と人数を出力する
            System.out.println(specialGuestsAvailableDates.first() + " " + M);
        } else {
            // 日付毎の通常ゲストの参加可能人数を集計
            for (int i=0; i<N; i++) {
                ArrayList<String> availableDates = getAvailableDates(scanner);
                for (String availableDate:availableDates) {
                    if (normalGuestsAvailableNumbers.containsKey(availableDate)) {
                        normalGuestsAvailableNumbers.put(availableDate, normalGuestsAvailableNumbers.get(availableDate) + 1);
                    } else if(M == 0) {
                        normalGuestsAvailableNumbers.put(availableDate, 1);
                    }
                }
            }

            // 通常ゲストの参加可能最大人数
            int maxNormalGuestsAvailableNumber = Collections.max(normalGuestsAvailableNumbers.values());

            // ゲストの合計参加人数
            int totalGuestsAvailableNumber = M + maxNormalGuestsAvailableNumber;

            // 通常ゲストの参加可能人数が最大かつ最速の日付と合計参加人数を出力する
            for (Map.Entry<String, Integer> normalGuestsAvailableNumber:normalGuestsAvailableNumbers.entrySet()) {
                if (normalGuestsAvailableNumber.getValue() == maxNormalGuestsAvailableNumber) {
                    System.out.println(normalGuestsAvailableNumber.getKey() + " " + totalGuestsAvailableNumber);
                    break;
                }
            }
        }
    }

    /**
     * ストリームから１行読み込み参加可能日一覧をリストで返す
     * @param scanner 入力ストリーム
     * @return 参加可能日一覧
     */
    private static ArrayList<String> getAvailableDates(Scanner scanner) {
        ArrayList<String> AvailableDates = readLineAsStringArrayList(scanner);
        AvailableDates.remove(0);
        return AvailableDates;
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineAsIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }

    /**
     * ストリームから１行読み込みString型のArrayListで返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ文字列の配列
     */
    private static ArrayList<String> readLineAsStringArrayList(Scanner scanner) {
        String line = scanner.nextLine();
        return new ArrayList<>(Arrays.asList(line.split(" ")));
    }
}
