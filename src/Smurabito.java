import java.util.*;

public class Smurabito {
    /**
     * 村人の人数
     */
    private static int N;
    /**
     * 友好関係の数
     */
    private static int M;
    /**
     * ログが更新される回数
     */
    private static int Q;

    /**
     * 村人
     */
    static class Villager{

        /**
         * 友好度のマップ<村人インデックス, 友好度>
         */
        Map<Integer, Integer> friendships;

        public Villager() {
            this.friendships = new HashMap<>();
        }

        /**
         * 友好度を設定する
         * @param villagerIndex 村人インデックス
         * @param friendshipPoint 友好度
         */
        void setFriendship(int villagerIndex, int friendshipPoint) {
            friendships.put(villagerIndex, friendshipPoint);
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final Integer[] villageInformation = readLineAsIntegerArray(scanner);

        // 村人の人数
        N = villageInformation[0];
        // 友好関係の数
        M = villageInformation[1];
        // ログが更新される回数
        Q = villageInformation[2];

        // 村人の配列
        Villager[] villagers = new Villager[N];
        for (int i=0; i<N; i++) {
            villagers[i] = new Villager();
        }

        // 友好度の配列
        Integer[][] friendships = readLinesAsInteger2dArray(scanner, M);

        // 各村人の友好度マップを設定する
        for (Integer[] friendship:friendships) {
            int villagerIndex1 = friendship[0] - 1;
            int villagerIndex2 = friendship[1] - 1;
            int friendshipPoint = friendship[2];

            villagers[villagerIndex1].setFriendship(villagerIndex2, friendshipPoint);
            villagers[villagerIndex2].setFriendship(villagerIndex1, friendshipPoint);
        }

        // ログ
        String[][] logs = readLineAsString2dArray(scanner, Q);

        // 同好会に入会している人の村人インデックスのセット
        TreeSet<Integer> friendshipParty = new TreeSet<>();

        for (String[] log:logs) {
            // 同好会を更新
            String action = log[0];
            int villagerIndex = Integer.parseInt(log[1]);
            if(action.equals("+")) {
                friendshipParty.add(villagerIndex);
            }else{
                friendshipParty.remove(villagerIndex);
            }

            //
            int[] maxFriendships = getMaxFriendships(friendships, friendshipParty);
            System.out.println(getMaxFriendshipPointOfParty(friendshipParty, maxFriendships));
        }
    }



    /**
     * 同好会のメンバーかどうか判定する
     * @param friendshipParty
     * @param villagerIndex
     * @return
     */
    private static boolean isMemberOfParty(Set<Integer> friendshipParty, int villagerIndex) {
        return friendshipParty.contains(villagerIndex);
    }

    private static int[] getMaxFriendships(Integer[][] friendships, Set<Integer> friendshipParty) {
        int[] maxFriendships = new int[N];
        for (Integer[] friendship:friendships) {
            int villager1 = friendship[0];
            int villager2 = friendship[1];
            int friendshipPoint = friendship[2];

            if(isMemberOfParty(friendshipParty, villager1) && isMemberOfParty(friendshipParty, villager2)
                    || !isMemberOfParty(friendshipParty, villager1) && !isMemberOfParty(friendshipParty, villager2)) {
                continue;
            }

            if(friendshipPoint > maxFriendships[villager1 - 1])
                maxFriendships[villager1 - 1] = friendshipPoint;

            if(friendshipPoint > maxFriendships[villager2 - 1])
                maxFriendships[villager2 - 1] = friendshipPoint;

        }
        return maxFriendships;
    }

//    private static int[] getMaxFriendships(Integer[][] friendships, Set<Integer> friendshipParty) {
//        int[] maxFriendships = new int[N];
//        for (Integer[] friendship:friendships) {
//            int villager1 = friendship[0];
//            int villager2 = friendship[1];
//            int friendshipPoint = friendship[2];
//
//            if(isMemberOfParty(friendshipParty, villager1) && isMemberOfParty(friendshipParty, villager2)
//                    || !isMemberOfParty(friendshipParty, villager1) && !isMemberOfParty(friendshipParty, villager2)) {
//                continue;
//            }
//
//            if(friendshipPoint > maxFriendships[villager1 - 1])
//                maxFriendships[villager1 - 1] = friendshipPoint;
//
//            if(friendshipPoint > maxFriendships[villager2 - 1])
//                maxFriendships[villager2 - 1] = friendshipPoint;
//
//        }
//        return maxFriendships;
//    }

    private static int getMaxFriendshipPointOfParty(Set<Integer> friendshipParty, int[] maxFriendships) {
        if (friendshipParty.size() == 0 || friendshipParty.size() == N)
            return 0;

        int maxFriendshipPoint = 0;
        for (int villagerIndex:friendshipParty) {
            int friendshipPoint = maxFriendships[villagerIndex - 1];
            if(friendshipPoint > maxFriendshipPoint) {
                maxFriendshipPoint = friendshipPoint;
            }
        }
        return maxFriendshipPoint;
    }

    /**
     * ストリームから１行読み込みInteger型で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値
     */
    private static Integer readLineAsInteger(Scanner scanner) {
        String line = scanner.nextLine();
        return Integer.parseInt(line);
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineAsIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }

    /**
     * ストリームから複数行読み込みInteger型の２次元配列で返す
     * @param scanner 入力ストリーム
     * @param numberOfReadLeadLines 読み込む行数
     * @return 読み込んだ数値の２次元配列
     */
    private static Integer[][] readLinesAsInteger2dArray(Scanner scanner, int numberOfReadLeadLines) {
        Integer[][] lines = new Integer[numberOfReadLeadLines][];
        for(int i = 0; i < numberOfReadLeadLines; i++) {
            lines[i] = readLineAsIntegerArray(scanner);
        }
        return lines;
    }

    /**
     * ストリームから１行読み込みString型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ文字列の配列
     */
    private static String[] readLineAsStringArray(Scanner scanner) {
        String line = scanner.nextLine();
        return line.split(" ");
    }

    /**
     * ストリームから複数行読み込みString型の２次元配列で返す
     * @param scanner 入力ストリーム
     * @param numberOfReadLeadLines 読み込む行数
     * @return 読み込んだ文字列の２次元配列
     */
    private static String[][] readLineAsString2dArray(Scanner scanner, int numberOfReadLeadLines) {
        String[][] lines = new String[numberOfReadLeadLines][];
        for(int i = 0; i < numberOfReadLeadLines; i++) {
            lines[i] = readLineAsStringArray(scanner);
        }
        return lines;
    }
}
