import java.util.Scanner;

public class NTT_DATA_D {

    private static String NORMAL = "A";
    private static String ERROR = "W";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // プログラム情報取得
        String[] programInformation = readLineAsStringArray(scanner);
        final String S = programInformation[0];
        final String N  = programInformation[1];

        if (S.equals(ERROR)) {
            System.out.println(N);
        } else {
            System.out.println(NORMAL);
        }
    }

    /**
     * ストリームから１行読み込みString型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ文字列の配列
     */
    private static String[] readLineAsStringArray(Scanner scanner) {
        String line = scanner.nextLine();
        return line.split(" ");
    }
}
