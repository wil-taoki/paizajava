import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class NDD_DATA_A_GENERATE_TEST {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String myColor = "0";
        String line = scanner.nextLine();
        if (line.equals("○")) {
            myColor = "1";
        }

        ArrayList<String> outputs = new ArrayList<>();
        line = scanner.nextLine();
        for (int i=0; i<line.length(); i++) {
            String outputLine = Arrays.stream(line.split("")).map(color -> {
                if (color.equals("○")) {
                    return "1";
                } else if (color.equals("◉")) {
                    return "0";
                }
                return "-1";
            }).collect(Collectors.joining(" "));
            outputs.add(outputLine);
            line = scanner.nextLine();
        }

        System.out.println(outputs.size() + " " + myColor);
        for (String outputLine:outputs) {
            System.out.println(outputLine);
        }
    }

    /**
     * ストリームから１行読み込みString型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ文字列の配列
     */
    private static String[] readLineAsStringArray(Scanner scanner) {
        String line = scanner.nextLine();
        return line.split(" ");
    }
}
