import java.util.Arrays;
import java.util.Scanner;

public class NTT_DATA_C {
    /**
     * プレゼントの個数
     */
    private static int N;
    /**
     * 新品のリボンの長さ
     */
    private static int L;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // プレゼント情報取得
        Integer[] presentInformation = readLineAsIntegerArray(scanner);
        N = presentInformation[0];
        L = presentInformation[1];

        // 現在使用しているリボンの残り長さ
        int currentRibbonLength = L;
        // 現在のリボン使用数
        int countRibbon = 1;

        // 各プレゼントをラッピングする
        for (int i=0; i<N; i++) {
            // リボンの長さが0になったら新しいリボンに交換
            if (currentRibbonLength == 0) {
                countRibbon++;
                currentRibbonLength = L;
            }

            // プレゼントをラッピングするのに必要となるリボンの長さを取得
            int useRibbonLength = readLineAsInteger(scanner);

            if (canUseThisRibbon(currentRibbonLength, useRibbonLength)) {
                // 現在使用しているリボンで長さが足りればそのリボンを使用
                currentRibbonLength -= useRibbonLength;
            } else {
                // 現在使用しているリボンで長さが足りなければ新しいリボンに交換して使用
                countRibbon++;
                currentRibbonLength = L - useRibbonLength;
            }
        }

        // リボンの使用数を出力
        System.out.println(countRibbon);
    }


    /**
     * 現在使用しているリボンでラッピング可能かどうかチェックする
     * @param currentRibbonLength 現在使用しているリボンの残り長さ
     * @param useRibbonLength プレゼントをラッピングするのに必要となるリボンの長さ
     * @return 現在使用しているリボンでラッピング可能かどうか
     */
    private static boolean canUseThisRibbon(int currentRibbonLength, int useRibbonLength) {
        return currentRibbonLength >= useRibbonLength;
    }

    /**
     * ストリームから１行読み込みInteger型で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値
     */
    private static Integer readLineAsInteger(Scanner scanner) {
        String line = scanner.nextLine();
        return Integer.parseInt(line);
    }

    /**
     * ストリームから１行読み込みInteger型の配列で返す
     * @param scanner 入力ストリーム
     * @return 読み込んだ数値の配列
     */
    private static Integer[] readLineAsIntegerArray(Scanner scanner) {
        String line = scanner.nextLine();
        return Arrays.stream(line.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
